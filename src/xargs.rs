extern crate getopts;
extern crate memchr;
extern crate sysconf;

use getopts::{Matches, Options, ParsingStyle};
use std::env;
use std::fs::File;
use std::i32;
use std::io;
use std::io::BufRead;
use std::process;
use std::str::from_utf8;

#[cfg(target_os = "linux")]
use sysconf::{sysconf, SysconfVariable};

struct ExecConfig {
    max_args_count: usize,
    max_cmdline_chars: usize,
    delimiter: u8,
    echo: bool,
    replace_str: Option<String>,
    prompt: bool,
}

struct CmdlineLimits {
    env_size: usize,
    min_allowed_arg_size: usize,
    max_args_size: usize,
    effective_max_args_size: usize,
    max_paralellism: usize,
}

#[cfg(target_os = "linux")]
fn get_system_max_argument_size() -> usize {
    sysconf(SysconfVariable::ScArgMax).unwrap() as usize
}

#[cfg(target_os = "windows")]
fn get_system_max_argument_size() -> usize {
    // https://support.microsoft.com/en-us/help/830473/command-prompt-cmd-exe-command-line-string-limitation
    8191 * 8
}

#[cfg(target_os = "linux")]
fn read_confirm_tty() -> bool {
    if let Ok(tty) = File::open("/dev/tty") {
        let mut ans = String::new();
        let mut tty_reader = io::BufReader::new(tty);
        if let Ok(len) = tty_reader.read_line(&mut ans) {
            assert!(len > 0, "expected non-empty read from tty");
            ans.starts_with("y")
        } else {
            eprintln!("Failed to read from tty, assuming false");
            false
        }
    } else {
        eprintln!("Failed to open /dev/tty");
        false
    }
}

#[cfg(target_os = "windows")]
fn read_confirm_tty() -> bool {
    eprintln!("n");
    eprintln!("Reading from TTY is not supported in this system, assumed 'n'");
    false
}

impl CmdlineLimits {
    fn load() -> CmdlineLimits {
        let max_args_size = get_system_max_argument_size();
        let env_size = env::vars().fold(0usize, |mut s, (k, v)| {
            s += k.len() + v.len();
            s
        });
        let effective_max_args_size = if max_args_size > env_size {
            max_args_size - env_size
        } else {
            eprintln!(
                "Maximum command line size allowed ({}) is smaller than env size ({})",
                max_args_size, env_size
            );
            0usize
        };
        CmdlineLimits {
            env_size: env_size,
            min_allowed_arg_size: 4096,
            max_args_size: max_args_size,
            effective_max_args_size: effective_max_args_size,
            max_paralellism: i32::MAX as usize,
        }
    }
}

fn read_until_fixed_buff<R: io::BufRead + ?Sized>(
    r: &mut R,
    delim: u8,
    buf: &mut Vec<u8>,
    max_buf_len: usize,
) -> io::Result<usize> {
    /*
    IDEA: this function will treat the internal buffer as if it was fixed size, not ever consuming
    more items than the array can store. In addition, `r`'s consume won't be called with a size
    that would exceed the capacity that can be stored by `buf`.

    To consider:
    - Asserting on `buf` being of size "zero", but with a non-zero positive capacity
    - How efficient is the use of `fill_buf` in this case? For a small `buf`, we might throw away
      a bunch of the bytes read and fill again on the next execution
    - We might want to simply extend_from_slice until full, but keep "position" intact otherwise 
    - How to represent end of read when there's no more room in `buf`?
    */
    let mut read = 0;
    loop {
        let (done, used) = {
            let available = match r.fill_buf() {
                Ok(n) => n,
                Err(ref e) if e.kind() == io::ErrorKind::Interrupted => continue,
                Err(ref e) if e.kind() == io::ErrorKind::Other => {
                    panic!("Got ErrorKind::Other when filling buffer")
                }
                Err(e) => return Err(e),
            };
            let buf_rem = if max_buf_len > buf.len() {
                max_buf_len - buf.len()
            } else {
                0
            };
            match memchr::memchr(delim, available) {
                Some(i) if i > buf_rem => (true, 0),
                Some(i) => {
                    buf.extend_from_slice(&available[..i + 1]);
                    (true, i + 1)
                }
                None if available.len() > buf_rem => (true, 0),
                None => {
                    buf.extend_from_slice(available);
                    (false, available.len())
                }
            }
        };
        read += used;
        r.consume(read);
        if done || used == 0 {
            return Ok(read);
        }
    }
}

fn read_extra_args<'a, R: io::BufRead>(
    config: &ExecConfig,
    reader: &mut R,
    line_buff: &'a mut Vec<u8>,
    max_buf_len: usize,
) -> Vec<&'a str> {
    let mut bytes_read: usize = 0;
    let mut indices: Vec<(usize, usize)> = Vec::new();
    for _ in 0usize..config.max_args_count {
        match read_until_fixed_buff(reader, config.delimiter, line_buff, max_buf_len) {
            Ok(0) => break,
            Ok(num_bytes) => {
                let new_sz = bytes_read + num_bytes;
                // last byte is not included, as it's the delimeter itself
                indices.push((bytes_read, new_sz - 1));
                bytes_read = new_sz;
            }
            Err(error) => {
                eprintln!("failed to read args: {}", error);
                break;
            }
        }
    }
    let line_buff = &*line_buff;
    indices
        .iter()
        .map(|&(i, j)| from_utf8(&line_buff[i..j]).unwrap())
        .collect()
}

fn get_input_delimeter(delimiter_opt: Option<String>, force_null: bool) -> Result<u8, String> {
    match (delimiter_opt, force_null) {
        (Some(_), true) => {
            Err("Options -0/--null and -d/--delimiter cannot be used at the same time".to_string())
        }
        (Some(ref x), false) if x.len() == 1 => Ok(x.bytes().next().unwrap()),
        (Some(ref y), false) => {
            //TODO: parse octals and hex
            match y.as_bytes() {
                b"\\n" => Ok(b'\n'),
                b"\\r" => Ok(b'\r'),
                b"\\t" => Ok(b'\t'),
                b"\\" => Ok(b'\\'),
                b"\\0" => Ok(b'\0'),
                b"\\a" | b"\\b" | b"\\f" | b"\\v" => Err(format!("Unsupported escape sequence: {}", y)),
                _ => Err(format!("Invalid input delimiter specification {}: the delimiter must be either a single \
                                 character or an escape sequence starting with \\.", y))
            }
        }
        (None, false) => Ok(b' '),
        (None, true) => Ok(b'\0'),
    }
}

fn get_max_args(opt: Option<String>) -> Result<usize, String> {
    match opt {
        Some(x) => match x.parse::<usize>() {
            Ok(n) => Ok(n),
            Err(_) => Err("Invalid number for -n option".to_string()),
        },
        None => Ok(5000), //maybe replace with something not hardcoded?
    }
}

fn get_max_cmdline_chars(opt: Option<String>, limits: CmdlineLimits) -> Result<usize, String> {
    match opt {
        Some(x) => match x.parse::<usize>() {
            Ok(x) if x > limits.effective_max_args_size => {
                eprintln!("-s value should be smaller or equal to {}", x);
                Ok(limits.effective_max_args_size)
            }
            Ok(n) => Ok(n),
            Err(_) => Err("Invalid number for -s option".to_string()),
        },
        None => Ok(limits.effective_max_args_size),
    }
}

fn execute_until_args_end<R: io::BufRead>(
    config: &ExecConfig,
    program_name: &str,
    initial_args: Vec<&str>,
    reader: &mut R,
) -> Result<(), String> {
    loop {
        assert!(config.max_cmdline_chars > program_name.len());
        let buff_capacity = config.max_cmdline_chars - program_name.len();
        let mut line_buff: Vec<u8> = vec![];
        let extra_args = read_extra_args(config, reader, &mut line_buff, buff_capacity);
        if extra_args.len() == 0 {
            return Ok(());
        }

        let args: Vec<&str> = match &config.replace_str {
            Some(s) => {
                let mut replaced: Vec<&str> = vec![];
                for elem in &initial_args {
                    if elem == s {
                        replaced.extend(extra_args.iter());
                    } else {
                        replaced.push(elem);
                    }
                }
                replaced
            }
            None => initial_args
                .iter()
                .map(AsRef::as_ref)
                .chain(extra_args)
                .collect(),
        };

        if config.echo && !config.prompt {
            eprintln!("{} {}", program_name, args.join(" "));
        }

        if config.prompt {
            eprint!("{} {}?...", program_name, args.join(" "));
            if !read_confirm_tty() {
                return Ok(());
            }
        }

        let exec = process::Command::new(program_name)
            .args(args)
            .stdout(process::Stdio::inherit())
            .stderr(process::Stdio::inherit())
            .status()
            .expect("Failed to execute command");
        if !exec.success() {
            let err = String::from(format!(
                "Child process returned: {}",
                exec.code().unwrap_or(-1)
            ));
            return Err(err);
        }
    }
}

fn run_from_args(matches: Matches) -> Result<(), String> {
    let limits = CmdlineLimits::load();
    let program = matches.free.get(0).map(AsRef::as_ref).unwrap_or("echo");
    let initial_args: Vec<&str> = matches.free.iter().skip(1).map(AsRef::as_ref).collect();

    let delimiter = get_input_delimeter(matches.opt_str("d"), matches.opt_present("0"))?;
    let max_args_count = get_max_args(matches.opt_str("n"))?;
    let max_cmdline_chars = get_max_cmdline_chars(matches.opt_str("s"), limits)?;
    let config = ExecConfig {
        max_args_count: max_args_count,
        delimiter: delimiter,
        echo: matches.opt_present("t"),
        max_cmdline_chars: max_cmdline_chars,
        replace_str: matches.opt_str("I"),
        prompt: matches.opt_present("p"),
    };

    match matches.opt_str("a") {
        Some(arg_path) => match File::open(arg_path) {
            Ok(arg_file) => {
                let mut reader = io::BufReader::new(arg_file);
                execute_until_args_end(&config, program, initial_args, &mut reader)
            }
            Err(err) => return Err(err.to_string()),
        },
        None => {
            let std_in = io::stdin();
            let mut reader = std_in.lock();
            execute_until_args_end(&config, program, initial_args, &mut reader)
        }
    }
}

fn print_usage(program: &str, opts: Options) {
    let brief = format!("Usage: {} [OPTION]... COMMAND [INITIAL-ARGS]...", program);
    print!("{}", opts.usage(&brief))
}

fn print_limits() {
    let limits = CmdlineLimits::load();
    eprintln!(
        "Your environment variables take up {} bytes
POSIX upper limit on argument length (this system): {}
POSIX smallest allowable upper limit on argument length (all systems): {}
Maximum length of command we could actually use: {}
Maximum parallelism (--max-procs must be no greater): {}",
        limits.env_size,
        limits.max_args_size,
        limits.min_allowed_arg_size,
        limits.effective_max_args_size,
        limits.max_paralellism
    );
}

fn get_option_parser() -> Options {
    let mut opts = Options::new();
    opts.parsing_style(ParsingStyle::StopAtFirstFree)
        .optflag("h", "help", "print this help menu")
        .optopt(
            "a",
            "arg-file",
            "read items from file instead of standard input.",
            "file",
        ).optopt(
            "d",
            "delimiter",
            "items in input stream are separated by CHARACTER",
            "CHARACTER",
        ).optopt(
            "n",
            "max-args",
            "use at most MAX-ARGS arguments per command line",
            "MAX-ARGS",
        ).optopt(
            "s",
            "max-chars",
            "limit length of command line to MAX-CHARS",
            "MAX-CHARS",
        ).optopt(
            "I",
            "replace",
            "replace R in INITIAL-ARGS with names read from standard input; if R is unspecified, assume {}",
            "R",
        )
        .optflag("p", "interactive", "prompt before running commands")
        .optflag("t", "verbose", "print commands before executing them")
        .optflag("0", "null", "items are separated by a null, not whitespace")
        .optflag("", "show-limits", "show limits on command-line length");
    opts
}

fn main() {
    let args: Vec<String> = env::args().collect();
    let opts = get_option_parser();
    match opts.parse(&args[1..]) {
        Ok(m) => {
            if m.opt_present("h") {
                print_usage(&args[0], opts);
                return;
            }
            if m.opt_present("show-limits") {
                print_limits();
            }
            if let Err(error) = run_from_args(m) {
                eprintln!("{}", error);
            }
        }
        Err(f) => eprintln!("{}", f.to_string()),
    };
}
